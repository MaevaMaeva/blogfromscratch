<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type= "text/css" rel="stylesheet" href="style.css">
    <title>Accueil</title>
</head>
<body>
    <?php
        //afficher les erreurs :
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    ?>
    <header>
        <?php
            //lier le header :
            include __DIR__.'/../includes/header.php';
            
            //lier le fichier fonction
            include __DIR__.'/../includes/fonctions.php';
        ?>
    </header>
    <main>
        <?php 

            //appeler la fonction nav (pour le login et l'option tri)
            nav();
            
            //appeler la fonction qui ouvre la page selon le tri :
            ouvertureIndex()
        ?>
    </main>
    <footer>
        <?php
            //lier le footer :
            include __DIR__.'/../includes/footer.php';
        ?>
    </footer>
</body>
</html>