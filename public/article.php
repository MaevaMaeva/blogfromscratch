<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link type= "text/css" rel="stylesheet" href="style.css">
            <title>Article</title>
        </head>
        <body>
            <?php
                // pour afficher les erreurs :
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);
            ?>
            <header>
                <?php
                    //lier le header :
                    include __DIR__.'/../includes/header.php';
                    
                    //lier le fichier fonction
                    include __DIR__.'/../includes/fonctions.php';
                ?>
            </header>
            <main>
                <!-- retourner à l'accueil grâce au lien 'retourner à l'accueil' -->
                <div>
                    <a href="/index.php">retourner à l'accueil</a>
                </div>
                <?php
                    //récupérer l'id de la page dans une variable :
                    $pageArticle = $_GET['id'];
                    
                    //lier le fichier sensible :
                    require ('configuration.php');

                    //accéder à la bdd :
                    $bdd = new PDO('mysql:host=localhost;dbname=blogfromscratch', $myUsername , $myPassword);
                    
                    //faire une requete pour récupérer les lignes de la table articles dans une variable :
                    $reponse = $bdd->query('SELECT * FROM articles JOIN authors ON articles.author_id = authors.id WHERE articles.id = '. $pageArticle);
                    //grâce au WHERE on récupère le bon article, cad celui qui a l'id de la page
                    //Grâce au JOIN on récupère la table auteurs en prime
                    
                    //mettre le contenu de notre table 
                    //sous forme d'objet dans une nouvelle variable :
                    $donnees = $reponse->fetch();
                    
                    //afficher le contenu de l'article
                    echo '<div class= "billet" id= '.$pageArticle.'>
                        <h2>'.$donnees['title'].'</h2>
                        <p class= "droite" >Date de publication : '.$donnees['published_at'].'</p>
                        <img src="'.$donnees['image_url'].'" alt="illustration article">
                        <h3>Auteur : '. $donnees['firstname'] .' '. $donnees['lastname'] . '</h3>
                        <p>'.$donnees['content'].'</p>
                        <p class= "droite" >Temps de lecture : '. $donnees['reading_time'].'</p>';
                    
                    //faire une requete pour récupérer la table catégories
                    $category = $bdd->query('SELECT * FROM categories JOIN articles_categories ON  articles_categories.category_id=categories.id  WHERE articles_categories.article_id = '. $pageArticle);
                    // grâce au XHERE on choisit les catégories qui correspondent à l'article
                    //grâce au JOIN on peut avoir accès à l'id de l'article

                    //boucler pour afficher toutes les catégories correspondant à l'article
                    foreach ($category as $cat)
                    {
                        echo '<p> Catégories : '.$cat['category'].'</p>' ;
                    };
                    echo '</div>';

                ?>
            </main>
            <footer>
                <?php
                    //pour lier le footer :
                    include __DIR__.'/../includes/footer.php';
                ?>
            </footer>
        </body>
    </html>