<?php
//fonction navigation
function nav ()
{
    ?>
        <nav>
            <!-- login -->
            <form method = "POST" action="">
                    <label for="username" >Username </label>
                    <input type="text" name="username" placeholder="Nom d'utilisateur">
                    <label for="password" >Password </label>
                    <input type="password" name="password" placeholder="Mot de passe">
                    <input type="submit" name="OK">
            </form>
            <!-- Triage -->
            <form action="">
                <div>
                    <label for="tri">Trier par </label>
                    <input type="submit" name="page" value="categories">
                    <input type="submit" name="page" value="auteurs">
                    <input type="submit" name="page" value="dates">
                    </br>
                    </br>
                    <input type="submit" name="page" value="retour à l'accueil">
                </div>
            </form>
        </nav>
    <?php 
};


//fonction pour afficher l'accueil, ordre par date la plus récente :
function mainAccueil ()
{
    //lier le fichier sensible :
    require ('configuration.php');
            
    //accéder à la bdd :
    $bdd = new PDO('mysql:host=localhost;dbname=blogfromscratch', $myUsername , $myPassword);
    
    //faire une requete pour récupérer les lignes de la table articles dans une variable :
    $reponse = $bdd->query('SELECT * FROM articles ORDER BY published_at DESC');

    //boucler pour afficher chaque ligne de la table, donc chaque article dans un squelette html :
    foreach ($reponse as $donnees)
    {
        // avec la fonction "strip_tags()" on Supprime les balises HTML et PHP du contenu de l'article 
        // pour pouvoir le couper au bout de 300 caractères par la suite  grâce à la fonction "substr()" 
        $content = strip_tags($donnees['content']);

        //afficher le contenu de chaque article
        echo '<div class= "billet" id='.$donnees['id'].'>
            <h2>'.$donnees['title'].'</h2>
            <p class= "droite">Date de publication : '.$donnees['published_at'].'</p>
            <img class = "preview" src="'.$donnees['image_url'].'" alt="illustration de l\'article">
            <p class = "cont">'.substr($content, 0, 300).'...</p>
            <p class= "droite"><a href="/article.php?id='.$donnees['id'].'">lire la suite</a></p>
            <p>Temps de lecture : '.$donnees['reading_time'].'</p>';

        //faire une requete pour récupérer la table auteurs  
        $auteur = $bdd->query('SELECT * FROM articles JOIN authors ON articles.author_id = authors.id WHERE articles.id='.$donnees['id']);
        
        //la mettre dans une variable sous forme d'objet grâce à fetch
        $aut = $auteur->fetch();
        
        //afficher l'auteur
        echo '<h3>Auteur : '.$aut['lastname']. ' '.$aut['firstname'].'</h3>';

        //faire une requete pour récuperer la table catégories
        $category = $bdd->query('SELECT * FROM categories JOIN articles_categories ON categories.id = articles_categories.category_id WHERE articles_categories.article_id = '. $donnees['id']);
        
        //boucler pour afficher toutes les catégories de l'article
        foreach ($category as $cat)
        {
            echo '<h4> Catégories : '.$cat['category'].'</h4>' ;
        };
        echo '</div>';
    };
};


//fonction pour trier par catégories :
function mainCat ()
{
    //lier le fichier sensible :
    require ('configuration.php');
                
    //accéder à la bdd :
    $bdd = new PDO('mysql:host=localhost;dbname=blogfromscratch', $myUsername , $myPassword);


    //faire une requete pour récupérer la table catégories  
    $cat = $bdd->query('SELECT * FROM categories ');
    
    //boucler pour afficher chaque ligne de la table catégories, donc chaque catégorie :
    foreach ($cat as $donneesCat)
    {
        echo '<div>';

        //afficher le titre de la catégorie
        echo '<h1>'.$donneesCat['category'].' : </h1>';
        
        //faire une requete pour récupérer la table articles en la liant aux catégories
        $reponse = $bdd->query ('SELECT * FROM articles JOIN articles_categories ON articles.id = articles_categories.article_id WHERE articles_categories.category_id='.$donneesCat['id']);
        
        //boucler pour afficher le contenu de chaque article
        foreach ($reponse as $donnees)
        {
            $content = strip_tags($donnees['content']);

            //afficher le contenu de chaque article
            echo '<div class= "billet" id='.$donnees['id'].'>
                <h2>'.$donnees['title'].'</h2>
                <p class= "droite">Date de publication : '.$donnees['published_at'].'</p>
                <img class = "preview" src="'.$donnees['image_url'].'" alt="illustration de l\'article">
                <p class = "cont">'.substr($content, 0, 300).'...</p>
                <p class= "droite"><a href="/article.php?id='.$donnees['id'].'">lire la suite</a></p>
                <p>Temps de lecture : '.$donnees['reading_time'].'</p>';
            
            //faire une requete pour récupérer la table auteurs  
            $auteur = $bdd->query('SELECT * FROM articles JOIN authors ON articles.author_id = authors.id WHERE articles.id='.$donnees['id']);
                    
            //la mettre dans une variable sous forme d'objet grâce à fetch
            $aut = $auteur->fetch();
                
            //afficher l'auteur
            echo '<h3>Auteur : '.$aut['lastname']. ' '.$aut['firstname'].'</h3>';
        };
        echo '</div>';
    };

};

//fonction pour trier par auteur
function mainAuteur()
{
    //lier le fichier sensible :
    require ('configuration.php');
                
    //accéder à la bdd :
    $bdd = new PDO('mysql:host=localhost;dbname=blogfromscratch', $myUsername , $myPassword);


    //faire une requete pour récupérer la table auteurs  
    $auteur = $bdd->query('SELECT * FROM authors ');
    
    //boucler pour afficher chaque ligne de la table auteur, donc chaque auteur :
    foreach ($auteur as $donneesAuteur)
    {
        echo '<div>';

        //afficher le nom de l'auteur
        echo '<h1>'.$donneesAuteur['lastname']. ' '.$donneesAuteur['firstname'].' : </h1>
            <h3>Auteur de ...</h3>';
        
        //faire une requete pour récupérer dans une variable les lignes de la table auteurs en liant la table articles  :
        $reponse = $bdd->query('SELECT * FROM authors JOIN articles ON authors.id = articles.author_id WHERE articles.author_id='.$donneesAuteur['id']);
         
         
        foreach ($reponse as $donnees)
        {
            // avec la fonction "strip_tags()" on Supprime les balises HTML et PHP du contenu de l'article
            // pour pouvoir le couper au bout de 300 caractères par la suite  grâce à la fonction "substr()"
            $content = strip_tags($donnees['content']);

            //afficher le contenu de chaque article
            echo '<div class= "billet" id='.$donnees['id'].'>
                <h2>'.$donnees['title'].'</h2>
                <p class= "droite">Date de publication : '.$donnees['published_at'].'</p>
                <img class = "preview" src="'.$donnees['image_url'].'" alt="illustration de l\'article">
                <p class = "cont">'.substr($content, 0, 300).'...</p>
                <p class= "droite"><a href="/article.php?id='.$donnees['id'].'">lire la suite</a></p>
                <p>Temps de lecture : '.$donnees['reading_time'].'</p>';
            
                //faire une requete pour récupérer la table catégories
                $category = $bdd->query('SELECT * FROM categories JOIN articles_categories ON  articles_categories.category_id=categories.id  WHERE articles_categories.article_id = '. $donnees['id']);
                // grâce au XHERE on choisit les catégories qui correspondent à l'article
                //grâce au JOIN on peut avoir accès à l'id de l'article

                //boucler pour afficher toutes les catégories correspondant à l'article
                foreach ($category as $cat)
                {
                    echo '<h4> Catégories : '.$cat['category'].'</h4>' ;
                    echo '</div>';
                };

        }

        echo '</div>';
    };
};

//ouvrir la page en fonction du tri :
function ouvertureIndex()
{    
    //par défaut
    if(!isset($_GET['page']))
    {
		mainAccueil ();
    } 
    else 
    {
        //si get mène à categories
        if($_GET['page'] == "categories")
        {
			mainCat ();
        }
        //sinon si get mène à auteurs
        elseif($_GET['page'] == "auteurs")
        {
			mainAuteur ();
        }
        //toutes les autres posssibilités (dont retour à l'accueil)
        else
        {
			mainAccueil ();
		}
	}
};